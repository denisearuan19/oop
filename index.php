<?php
require('animal.php');

// index.php
$sheep = new Animal("shaun");

echo  "Name : $sheep->name <br>"; // "shaun"
echo  "Legs:  $sheep->legs<br>"; // 4
echo "Cold Booded:$sheep->cold_blooded<br>"; // "no"
echo "</br>";
echo "</br>";
$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>";
echo "Legs: $sungokong->leg <br>";
echo "Cold Booded: $sungokong->cold_blooded <br>";
echo $sungokong->yell(); // "Auooo"
echo "</br>";
echo "</br>";
$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>";
echo "Legs : $kodok->leg <br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
echo $kodok->jump(); // "hop hop"